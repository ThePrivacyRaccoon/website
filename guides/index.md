# Privacy Guides

This section is still a work in progress. We will be writing more guides in the future. You may also contribute your own.

## Newcommers

* [An Easy Introduction to Password Managers](password-managers.html) - A simple to follow guide explaining what password managers and how to use them.

## Threat Modeling

* [Threat Modeling: The three-level template](threat-modeling-three-level-template.html) - An easier and simple approach for threat modeling that encourage taking action right now.
* [Commentary on Threat Modeling](threat-modeling-commentary.html) - We take a look at EFF's formula on threat modeling and the four-level template.

## Instant Messengers

* [Instant Messenger Comparison and Analysis](/im.html) - In-depth analysis at multiple IMs, analyzing them from our firm privacy-focused criteria. We compare them in various aspects in a table, for a quick overview.

## Browser

-- WIP --
