# Commentary on Threat Modeling

### Author's note:

The most popular formula for determining your threat model was first defined by **[EFF's Surveillance
Self-Defense Guide](https://ssd.eff.org/en/module/your-security-plan)**.
I'll explain this formula based on their guide and at the end of the article
we'll take a look at a different approach.

# Threat Modeling: EFF's approach

What's threat modeling? It consists on deciding which level of security works better for you,
trying to balance security, privacy and convenience of use.

To determine what's your threat model, you'll need to answer these questions:

* What do I want to protect?
* Who do I want to protect it from?
* How likely is it that I will need to protect it?
* How bad are the consequences if I fail?
* How much trouble am I willing to go through to try to prevent potential consequences?

Let's go through each of the questions to try to evaluate your threat model.

## What do I want to protect?

Things that you want to protect are usually referred to "assets". In your physical house,
your assets would be photo albums, jewelry, cash, gold, financial or official documents. 

Applied to digital privacy, we can distinguish between physical and non-physical assets.
Your laptop or your mobile phone are physical assets. Non-physical assets include any
kind of sensitive data: email account, bank account, passwords, intimate or family photos, 
location data, the websites you visit, etc.

It's a good idea to make a list with things you'd like to protect.

## Who do I want to protect it from?

You need to know which bad actors are you concerned about. Distinct adversaries require distinct
defense strategies.

You need to differentiate between the adversaries that will target you and those that won't.
For example, the average cybercriminal won't specifically target you as an individual (unless
you're someone of mayor importance or a public figure). On the other hand, your boss, your
former partner, business competition, law enforcement if you're an activist or a journalist,
etc.

Try to create a list of adversaries, this could include government agencies, corporations or
individuals.

Depending on your adversary, you should consider writing this list down in paper and burning it
down later.

## How likely is it that I will need to protect it?

Now that you know your assets and your adversaries, you need to determine the likelihood
of a threat against a certain asset might occur. We refer to the likelihood of a certain
threat as the risk.

You need to decide which assets are you going to protect seriously and which ones may be
too rare or difficult to protect from that they are not worth the effort.

## How bad are the consequences if I fail?

It depends completely on your assets and adversaries.

What would be the consequences if one of your adversaries found that you regularly visit
porn sites? (or any other kind of site that may embarrass you). If a potential future employer
discovers such a thing might result in not getting the job.

Maybe you're escaping from an abussive former partner can't permit themselves to make public
any location data, their home address or phone number.

Would you be okay if your cloud provider had a security breach and all your intimate photos,
potentially including nudity, were stolen and filtered? Or even worse, someone extorts you and
threats with sending the photos to your family and friends.

Are you an activist fighting against an opressive government? Then your mobile phone provider
which has access to most of your digital data will provide everything they need. Other
services like browsers, search engines, social media, cloud storage, etc will also share your
personal data with government agencies if requested.

Security planning involves understanding how bad the consequences could be if an adversary
successfully gains access to one of your assets. To determine this, you should consider the capability
of your adversary. As said earlier, your government would have access to all your phone records,
search history and much more. A cybercriminal in a public WiFi network may easily setup an attack
to stole any credentials: bank account, social media, etc. They could steal your money or
impersonate you.

## How much trouble am I willing to go through to try to prevent potential consequences?

Knowing your assets, your adversaries, your consequences and your risks, it's the time for balancing.
Evaluate the costs and the benefits for your security model. Some strategies provide higher security
and privacy, but they require learning to use new tools, something that some people may don't want to do.

Your primary goal should be to mitigate the most harmful and likely threats, while trying to reduce
the attack surface for the rest of the threats with general good security practices without impacting
your convenience too much.

# Threats modeling: The four-level template

There's a fairly popular article from [Cupwire](https://www.cupwire.com/threat-modeling/)
that suggest a four-level template. The article differentiates between these levels:

1. I want to keep my information private from friends and family.
2. I want to keep my information private from corporations.
3. I want to keep my information private from private investigators, bounty hunters, and "hackers".
4. I want to keep my information private from my city/state/alphabet agencies/foreign governments.

It's a great article, but I do not completely agree. I'll briefly explain my reasons here,
although they are worth their own article:

First of all, **if you're in this website reading this, it's because digital privacy concerns you**. And the Tier 1
will not provide you real privacy. It should be used together with Tier 2.
The second level and their actions to take to protect your data from corporations aren't good enough. Using
other tools will protect your personal data better with very little convenience loss. For example, using Librewolf
instead of Firefox does not suppose any convenience loss. 
Their first and second level should be merged, because their second level is somethig that affects everyone to
a greater or lesser extent. Governments and corporations shouldn't spy on the population. **Mass surveillance
is an outrage against freedom and democracy**. And every user of electronic devices is affected. 

# Our solution approach to threat modeling: the three-level template

We've improved the four-level template and created **[the three-level template](/guides/threat-modeling-three-level-template.html)**. 
Read through it, it's a good complement to the EFF's formula and it has some nice example actions to take
in order to start taking measures right now. 

Our intention was to simplify the decision. Choosing a level of security
is as simple as reading the summary of each level, while improving on Cupwire's levels.

**I'm not saying that our threat modeling template is the best**.
EFF's is longer and can more accurate reflect your exact needs. But we found all the new terms
and the questions to be too much information to digest. We've opted for a more direct approach:
choose one level between three, read a short description of the target group and take a look at the
example actions that you can start making right now!

It's up for each one
to decide their threat model the way they want. Each formula is useful for each for a different
type of person.
