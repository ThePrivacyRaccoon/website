# Phones can't be private

I'd like to elaborate much more on this subject, but for now, here's a message from Stella
in the Spyware MUC:

> They have KYC on most SIM cards and all IEMIs are scanned and tracked. You'd literally have to remove the baseband modem entirely before you can even make the argument that they are private or even secure for that matter. As basebands themselves are likely backdoored at behest of government agencies. As manufacturing of them is completely and hopelessly monopolized when talking about the actual chipsets. For GSM basebands (3G and below) there's Mediatek and LTE modems are manufactured by Qualcomm. The reason why competitors can not get in is actually patents. They control the whole market
