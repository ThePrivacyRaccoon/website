# Donations

* [Monero (XMR)](monero:82VZQatwUYXBpVPxk9m1fRidU3BYt9TVPFqJvLsduKR3jEvpGvhKM42icGt4Aan4SwTiYKp9V9Bf94u6dB6Xzg6kMRTdP8u) ![Monero Logo](/images/monero.png) 82VZQatwUYXBpVPxk9m1fRidU3BYt9TVPFqJvLsduKR3jEvpGvhKM42icGt4Aan4SwTiYKp9V9Bf94u6dB6Xzg6kMRTdP8u

![Monero QR](/images/monero-qr.png) 

## What donations are used for

This website is hosted, written and maintained by volunteers. Of course, there are no ads nor analytics. We will never include affiliate links. Never.
We don't accept corporate sponsors either. We think that if you receive money from a certain service you'll start
pushing it, regardless of your reader's privacy. This behaviour is [common in other "privacy sites"](/the-problem.html).

This site is hosted using the pocket money of it's creators. Donations will go for them so they can buy themselves
a coffee or a beer. If we start receiving donations more regularly we'll make sure to create a Transparency Report with the
expenses.

## Our own privacy services

If we start receiving enough regular donations, we have plans to start hosting our own services open for anyone.
Some services that we want to host and with we have experience are:

* XMPP server that federates through Tor and I2P
* Tor exit relay
* VPN or Proxy (Shadowsocks) with zero logs (if support and demand is enough, easily abused)
* SearX/SearXNG with Tor mirror
* Floodfill i2pd router
* Croc relay for file transfer
* Git repository hosting

We would make sure to run these with the most paranoid privacy and security measures. If we do offer services,
we would like them to be of the highest quality. That's why some donations
are needed since that would require good maintenance work.
Some security and privacy measures that we would make use of are:

* Everything will run inside an full disk encrypted hypervisor. We will be very cautious with the key
* Tor and I2P access when applicable
* No open ports for public SSH, the server will be only reachable through a VPN
* The OS will be security hardened
* Good system administration practices will be used 
