#  The Privacy Raccoon Report

In our podcast, we cover everything related to online privacy. From guides, new regulations, controversies, scandals and more.
Hosted by Reginald, our podcast is done in our spare time and with our pocket money, we don't accept corporate sponsors nor affiliated links.

![](images/COVER.png)
