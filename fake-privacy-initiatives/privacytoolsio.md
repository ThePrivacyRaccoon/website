# PrivacyTools.io

## UPDATE: PrivacyToolsIO has completely failed, read [here](/guides/privacytoolsio-nordvpn.html).

Probably the most popular privacy site. I used to visit this site too when I was getting started.

This site contains affiliate links and discount codes. While we understand that maintaining 
something like this requires a lot of time and effort, we consider that the addition of affiliate links and discount codes for certain services may affect the criteria for adding or removing that
service, or listing them instead of others which do not give them profits.

Furthermore, this site recommendations are very poor, often targeted at non technical people
thus sacrificing privacy. But in some cases they recommend privacy violators instead of alternatives
which would require the exact same effort to set up! We consider this unacceptable. This applies
to almost every site in this page.

First problem: It's [Cloudflared](https://git.nixnet.services/you/stop_cloudflare). Cloudflare is
a well known privacy violator and it's service acts as a MITM attack.

The are a lot of issues with their recommendations. For example, they recommend browsers which are
known to send telemetry like Firefox or Brave instead of the mitigated Librewolf and Ungoogled Chromium.
Additionally they recommend the Duckduckgo browser when it is clearly outclassed as a Chromium browser
by Ungoogled Chromium.

* They list Cloudflare as a DNS provider, when Cloudflare is a well known privacy violator.
* They recommend Mega as a "cloud" storage, when it's completely proprietary. They've recently had a [critical security flaw](https://www.bleepingcomputer.com/news/security/mega-fixes-critical-flaws-that-allowed-the-decryption-of-user-data/). I think that at this
point Mega is only listed because it's an affiliated link.
* In the email providers, they recommend Tutanota and Protonmail which don't allow third party email clients
and have other red flags.* Duckduckgo, Startpage, Swisscows and Brave search are recommended. All of them are bad for their own reasons.
* They recommend Thunderbird which [does not respect your privacy](/fake-privacy-initiatives/thunderbird.html).
* In Instant Messengers, they recommend Signal and Threema. The first one requiring a phone number and the second
one has a proprietary server. Both of them centralised.
* In their addons recommendantion, they recommend uBlock Origin and there's no mention of the more powerful uMatrix/eMatrix.

There's probably more, but I didn't bother. The fact that it is Cloudflared shows how little they care.
There was a huge drama months ago between PrivacyTools.io and PrivacyGuides.org which seemed to be
about the donations money. Maybe they only care about donations and that's why they list awful options like
Signal, Thunderbird or Cloudflare. Or maybe they are just incompetent. I don't know, but it sucks.
And it was one of the reasons for the creation of our site.
