# Surveillance Report Podcast (Techlore and The New Oil)

*UPDATE: They've changed their website and moved from the third party service that they used since. Still,
the content is not better and this is still relevant.*

The [Surveillance Report](https://surveillance-report.castos.com/) podcast 
seem to be the most popular podcast about digital privacy. It's presented by Techlore (a popular-ish
youtuber) and The New Oil (the next site on this list).

Before talking about the content, why don't we look at their site? ([archive](https://archive.ph/eWFIs))
The first thing we see is that they invite us to listen to their podcast in three platforms:

* Spotify
* Apple
* Pocketcasts

These privacy advocators are inviting you to listen their podcast in three data mining, centralized platforms.
If you think this is for visibility and to extend the privacy awareness, that may sound reasonable. But if you just
landed on their website they could direct you to a private way to listen them. After all, you're already on their site. 
A Spotify user will find them using the Spotify app, same for the other platforms.

Then why? Simple: they prioritize money over the privacy of their followers. Sure, they need to make a living. But they're
accepting recurrent donations from followers. Anyway, they still spy on them. 
I find ironic that they profit reporting mass surveillance using mass surveillance platforms. 
Quite hypocrite if you asked me.

Yes, you may also listen to their podcast by visiting their website or through the RSS feed. This would be great if
their website didn't make third party connections to Google. Yes, the website connects to Google Fonts.
Maybe Techlore didn't find your browser default fonts round enough to make the podcast attractive and
he decided to use Google's, at the expense of their visitors connecting to Google's servers.

Did I mention that they use a [third party service](https://castos.com/privacy/) to host their podcast?
Creating a podcast is as simple as uploading the audio files and creating an RSS feed. But they preferred
to rely on a third party which has it's own privacy policy. Their website seem to be built by Castos
and so their JavaScript is proprietary. This is an issue if you try to listen to their podcast through
their site, since their audio player won't work unless you enable JavaScript.

The content of the podcast is not much better than the site. A great example is that they seem to love
[Apple's marketing campaign](https://surveillance-report.castos.com/podcasts/7230/episodes/hardened-apple-devices-are-here-sr94).
In case you weren't aware, iOS is a proprietary OS which continously spies on it's users. 
Think of iOS like a blackbox with a nice, glossy cover. Apple may use
whatever marketing strategy that their huge marketing team think that sounds good. But **iOS is incompatible
with privacy**. No matter what they tell you or how many millions they invest in marketing campaigns.

But hey, these guys have the audacity to call themselves "authorities in the field [of privacy]" ([in this episode](https://surveillance-report.castos.com/podcasts/7230/episodes/here39s-exactly-how-we-stay-updated-with-privacy-news-sr-bonus-discussion))
I must admit that they're fairly talented as comedians. As so-called privacy authorities they either suck or they
take the mainstream approach because money is all they care about and their objective is to
to gather as much listeners as they can to make profit while putting privacy aside. Which is what I think they do,
because they look like competent people.
