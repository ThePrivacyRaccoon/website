# Techlore

We've already talked about his [podcast](/fake-privacy-initiatives/surveillancereport.html). And there's little difference with his channel and "privacy community".

Techlore recommends thing just because they have the word "privacy" in their marketing: Apple, Duckduckgo, Signal, Brave
and a long etc. Meanwhile they make people to use Discord to join and chat in their "privacy community".
They actively encourage people to use Android stock ROMs instead of the degoogled and security hardened ones like
DivestOS or GrapheneOS.

The fact that he uses Discord is incredibly suspicious. No self-respecting "privacy" channel should be hosting their
community in Discord. The only logic behind that is that by using Discord they gather to a greater audience so they can
make more profit, while the people that is actually concerned about privacy is forced to use Discord in order to join.