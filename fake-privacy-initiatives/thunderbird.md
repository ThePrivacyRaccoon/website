# Mozilla Thunderbird

Thunderbird contains a lot of spyware features, however all of these can be opted-out of and most of the spyware is connected to the web-browsing capabilities of Thunderbird. Thunderbird contains some minor spyware protection to its users and does not attempt to collect any information that is extremely sensitive, however it is spyware and does share and collect user information by default that it does not need to share.


From Thunderbird's [Privacy Policy](https://www.mozilla.org/en-US/privacy/thunderbird/) ([archive.org](https://web.archive.org/web/20230918154134/https://www.mozilla.org/en-US/privacy/thunderbird/)):

* **Thunderbird sends your IP to Amazon**, a major privacy violator:

> Thunderbird uses Amazon Web Services (AWS) to host its servers and as a content delivery network. Your device’s IP address is collected as part of AWS’s server logs.

* Thunderbird sends your email address to external servers:

> Thunderbird tries to determine the correct settings for your account by contacting Mozilla’s configuration database as well as external servers. These include DNS servers and standard autoconfiguration URIs. During this process, your email domain may be sent to Mozilla's configuration database, and your email address may be disclosed to your network administrators.

It is important to note that they mention "Mozilla's configuration database as well as **external servers**". Which external servers? It is not specified. Other email clients have this configuration database locally and thus do not query any server.

* It's also notable to highlight that Thunderbird has a partnership with Gandi.net and Mailfence, promoting them inside the program. These two providers are not privacy friendly and a private client shouldn't promt users to use these providers.

## Security Issues

Thunderbird has web-browsing capabilities and that opens a big door for security issues. From JavaScript, malicious HTML to remote content, there's a lot of attacks that [have made Thunderbird vulnerable](https://web.archive.org/web/20230918154239/https://www.cvedetails.com/vulnerability-list/vendor_id-452/product_id-3678/Mozilla-Thunderbird.html?page=1&order=1&trc=1276&sha=91c2558ef8a5e9b3792238fef31eec4bb0df23c3) in the past.

One notable attack, [efail](https://efail.de/) ([archive.org](https://web.archive.org/web/20230918154523/https://efail.de/)), exploited vulnerabilities in the OpenPGP encryption standard to reveal the plaintext of encrypted emails **abusing the use of the HTML content in emails**. Thunderbird was one of the major email clients vulnerable to this attack:

![efail table](/images/efail-table.png)

Our [recommended email clients](/index.html#email-clients) were not vulnerable to this (and other) attack.

## Other Telemetry

Additionally, some other telemetry that Thunderbird has enabled by default is specified in their Privacy Policy. You might opt out of most of these in the settings, but it is enabled by default:

> Thunderbird collects telemetry data by default to help improve the performance and stability of Thunderbird.

> Thunderbird receives data about your interactions with the application, such as whether calendars and filters are being used, and how many email accounts a user has.

> Thunderbird also receives basic information about your device and application version, including, hardware configuration, device operating system, and language preference. When Thunderbird sends technical data to us, your IP address is temporarily collected as part of our server logs.

> Thunderbird receives your email address domain. Your full email address is never processed or stored on our servers (unless you choose to share it when you send a crash report).

> Thunderbird also receives information about the application’s version and device operating system. When Thunderbird sends technical data to us, your IP address is temporarily collected as part of our server logs.

> To ensure you have the most up-to-date version of the product, Thunderbird checks for updates by periodically connecting to Thunderbird’s servers. Your application version, language, and device operating system are used

> Thunderbird periodically checks for blocklisted add-ons. Your Thunderbird version and language, device operating system, and list of installed add-ons are needed

> Thunderbird periodically connects to our server to install updates to add-ons. Your installed add-ons, application version, language, and device operating system are used

#### Sources:

* [Thunderbird's Privacy Policy](https://www.mozilla.org/en-US/privacy/thunderbird/) [archive.org](https://web.archive.org/web/20230918154134/https://www.mozilla.org/en-US/privacy/thunderbird/)
* [Efail.de](https://efail.de/) [archive.org](https://web.archive.org/web/20230918154523/https://efail.de/)
* [Online Spyware Watchdog](https://spyware.neocities.org/articles/thunderbird) [archive.org](https://web.archive.org/web/20230918155554/https://spyware.neocities.org/articles/thunderbird)
