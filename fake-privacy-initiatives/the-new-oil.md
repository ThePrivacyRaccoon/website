# The New Oil

When talking about the [Surveillance Report Podcast](/fake-privacy-initiatives/surveillancereport.html) we've already seen that The New Oil guy doesn't prioritize his followers privacy, but profit.
Let's take a look at his website too. 
This is how the site describes itself.

> This site is designed to help readers take back control of their data and regain their privacy online.

Sounds nice. If only it was true.
The main problem is that The New Oil targets too much non technical users. They recommend iOS. iOS is a 
proprietary blackbox
which can't be fixed. At least Android can be degoogled and you are able to mitigate most of it's
spyware. But with iOS you can't. They don't mention Android custom ROMs which are far more privacy
respecting than the factory one. Even Android stock can be degoogled using adb, something
which can't be done on iOS. 

They critize Firefox and Brave but still recommend them, not mentioning the truly private alternatives
like Librewolf and Ungoogled Chromium/Bromite.
*UPDATE: They list Librewolf as an honorable mention below, still recommending Firefox and Brave.*

For email, once again, they recommend Tutanota and Protonmail. One fun fact is that they give Protonmail
more "Pros" and less "Cons" than Tutanota. Casually they happen to link Protonmail with an affiliate link
while they don't have one for Tutanota.

As instant messengers they recommend Signal, Threema and Wire. Three centralized services which locks you in.
You're being forced to trust them despite of all of them having serious flaws, for example, Signal app comes
with proprietary Google libraries in Android, they include some blockchain non-sense mobilecoin in their app
and the worst red flag is that they require you to use a phone number. Threema's server
is proprietary, we can't know what are they running. *UPDATE: they do not recommend Threema anymore, but
Signal and Wire are still listed.*

There are probably more issues but I don't want to waste more time in that site. It's also interesting that they are hosting
their site in Gitlab which is Cloudflared and won't work at all without JavaScript. This says a lot about
them.