# PrivacyGuides

## Infrastructure

Before analyzing their content, let's take a look at their infrastructure.

### Github and "Open Source"

I'd like to start with a quote of how the project describes themselves:

> Privacy Guides is a non-profit, socially motivated website that provides information for protecting your data security and privacy.

They also love to repeat how they are an "open source" project, which is great. I like privacy and I like open source, so let's try to contribute to said open source project. They'll send us to their Github page. Ouch. So, you're telling me that I need to create an account in a [Microsoft owned, vendor-locking service](https://sfconservancy.org/GiveUpGitHub/) in order to help others to protect their privacy. Ironic. 

In case you didn't care about creating an account in a service like Github that blocks registrations with Tor and VPNs, there's more. 

> This website uses [mkdocs-material-insiders](https://squidfunk.github.io/mkdocs-material/insiders/) which offers additional functionality over the open-source mkdocs-material project. For obvious reasons we cannot distribute access to the insiders repository. Running this website locally without access to insiders is unsupported.

Oh so they intentionally lock generating their website locally against a paywall and proprietary software. They are making contributions harder by not letting contributors to generate their changes in their local environment. They also make it harder use the code of their website, which is under the MIT license, but paywalled and requires a proprietary program. So yeah, they are super open source.

One might think that surely they aren't doing this on purpose. They wouldn't lock their website with a paywall and proprietary software so they eliminate possible competition. Well, I invite you to read the next section on licensing and make some connections.


### Licensing

PrivacyGuides was born as a new project by the old staff of the PrivacyTools website. The [PrivacyTools](/fake-privacy-initiatives/privacytoolsio.html) site used either the [CC0](https://web.archive.org/web/20200215071440/https://www.privacytools.io/LICENSE.txt) license or the [WTFPL](https://web.archive.org/web/20230112072540/https://www.privacytools.io/WTFPLV2.txt) ("DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE"), both licenses provide everyone the freedom to do whatever they want with the content, making it effectively public domain.

From it's conception to September 2022, PrivacyGuides was under the CC0 too, until the founder of the project [relicensed the whole website](https://github.com/privacyguides/privacyguides.org/commit/3ab0c984e8a39063704ef66cf8f019bcbb9803b0) ([archive.org](https://web.archive.org/web/20230918174311/https://github.com/privacyguides/privacyguides.org/commit/3ab0c984e8a39063704ef66cf8f019bcbb9803b0)) under the [Creative Commons Attribution-NoDerivatives 4.0 International Public License](https://github.com/privacyguides/privacyguides.org/blob/main/LICENSE). 

Let me provide you with an excerpt of how this license works, from the Creative Commons website.

> No Derivatives licenses (CC BY-ND and CC BY-NC-ND) allow people to copy and distribute a work but prohibit them from adapting, remixing, transforming, translating, or updating it, in any way that makes a derivative. In short, people are not allowed to create “derivative works” or adaptations. 

Am I the only one seeing the issue here? We're privacy advocators and researchers, we publish to be read, to have impact, and to make the world a better place. To accomplish these important goals, researchers need to enable reuse and adaptations of their research publications and data. They also need to be able to reuse and adapt the publications and data of others.

ND licensed publications are not Open Access. ND licenses overly restrict reuse of content by fellow researchers and thus curtail their opportunity to contribute to the advancement and sharing of knowledge.

Returning to our topic, this is from PrivacyGuides about page:

> Our mission is to inform the public about the value of digital privacy, and global government initiatives which aim to monitor your online activity.

I start wonder if PrivacyGuides is more focused on restricting access and redistribution of information and maintaining similar sites at bay in order to gather the donations from the privacy-minded people instead of their stated goal of informing people. Sure, they have a beautiful mission statement. But their license, their use of Github and proprietary software tell otherwise.

*For more on the licensing issue, I recommend reading this [excellent article](https://creativecommons.org/2020/04/21/academic-publications-under-no-derivatives-licenses-is-misguided/) ([archive.org](https://web.archive.org/web/20230918172645/https://creativecommons.org/2020/04/21/academic-publications-under-no-derivatives-licenses-is-misguided/)) by Brigitte Vézina, which I used to write this section. It comments on the issues with the no derivatives license.*

### Cloudflare

Cloudflare is a well known privacy violator which acts as a MITM with their proxy for a huge part of the internet, blocking people who use Tor or VPNs, attacking everyone's right to privacy. It would be a weird choice for so-called privacy advocators to use Cloudflare, wouldn't it? Well, let's see:

* If we check their expenses on their OpenCollective page (where they receive donations), we can see [multiple payments to Cloudflare](https://web.archive.org/web/20230918165700/https://opencollective.com/privacyguides/expenses?limit=10&searchTerm=cloudflare). Most of them are around $10 and are listed as domain payment, so they are using Cloudflare as their registrar. They also made a [$79.62 payment](https://web.archive.org/web/20230918170319/https://opencollective.com/privacyguides/expenses/74201) to Cloudflare stating "Registrar Renewal Fees - includes iCANN Fee (extended to 2032)", with date of Apr 26, 2022 and one might think that they've paid for several years of domain ownership for that quantity of donations money, but still they paid again for renewal of domains to Cloudflare in 2023 [two times](https://web.archive.org/web/20230918170634/https://opencollective.com/privacyguides/expenses?limit=10&searchTerm=cloudflare&collectiveSlug=privacyguides&period=2023-01-01T00%3A00%3A00.000Z%E2%86%92all). This could be explained if they hold a big number of domains, which I feel totally unnecessary for the project, but it's their donators money, not mine.
* PrivacyGuides.org [is using Cloudflare's DNS](/images/privacyguides-cloudflare.png). 
* PrivacyGuides.net, where they provide their services, is using [Cloudflare's DNS and either their CDN or Proxy](privacyguides-cloudflare2.png).



## Content

Created by a former maintainer of PrivacyTools.io. Let's see
if it's any better than PrivacyTools in their recommendations:

* **Cloudflare's DNS is [recommended](https://www.privacyguides.org/dns/)** ([archive.org](https://web.archive.org/web/20220824021919/https://www.privacyguides.org/dns/), [archive.is](https://archive.ph/3T1NY)).
* Firefox and Brave are recommended instead of Librewolf and Ungoogled Chromium.
* Additionally, they recommend Safari which is proprietary and spies on you.
* They recommend [Thunderbird which continously phones home](/fake-privacy-initiatives/thunderbird.html). It has a history of being vulnerable to major email attacks like [efail](https://efail.de/media/img/efail-disclosure-smime.png).
* Tutanota and Protonmail are in the email provider list.
* Fake privacy initiatives like Duckduckgo and Startpage are recommended.
* They are recommending proprietary
software, such as Apple Mail and Canary Mail! ([archive.org](https://web.archive.org/web/20220824020934/https://www.privacyguides.org/email-clients/), [archive.is](https://archive.ph/QvmHU))
This is a **big red flag**. These programs are black boxes, we do not know what are they doing at all. The
user loses the control. **We, as privacy activists, should have high standards and do not allow recommending
software that violates users privacy**.
This alone shows that there's no real privacy here. Yet another privacy website
which instead of digging deeper for real private alternatives they recommend
the mainstream solutions. They go as far as recommending proprietary spyware like Apple Mail or Safari.

This does not make PrivacyGuides useless, there are still good recommendations there. But **they 
recommend well-known privacy violators like Cloudflare** and proprietary software like Apple Mail and Canary Mail.
**This should not be tolerated** by privacy advocators. There are alternatives much more private that are just as easy to use.

What extra work does it cost the user to switch to a private DNS instead of switching to Cloudflare? If someone is changing their DNS servers
anyway, why do they recommend Cloudflare instead of a better provider? You have to follow exactly the same process
to change your DNS servers, no matter if it's Cloudflare's or Mullvad's. This applies to other recommendations they
make and we can't understand the reasoning behind such decisions.

*UPDATE2*: They are now recommending [1Password](https://www.privacyguides.org/passwords/#1password) ([archive.org](https://web.archive.org/web/20220905025212/https://www.privacyguides.org/passwords/#1Password), [archive.is](https://archive.ph/nCRgh)).
They give a bunch of reasons, yet we can't understand why are they recommending this. 1Password is a proprietary
password manager whose main feature is cloud sync (Bitwarden does exactly the same being free/libre software). Oh and
1Password is also paid, no free tier. This is definetely going to be a drawback for non technical users, which by reading
their arguments seem to be the target public for this recommendation. Bitwarden has an excellent free tier.
They mention that one advantage of 1Password over Bitwarden is excellent support for native clients. Bitwarden also has
native clients, but I don't think that matters because a non technical user will just use the web version anyway,
people does everything on their browser nowadays.

To sum up, we can't find a good reason for recommending the **proprietary 1Password** when you're already recommending
Bitwarden. The difference in terms of features is minimal, the dealbreaker is that Bitwarden is free/libre software while
1Password is proprietary.
