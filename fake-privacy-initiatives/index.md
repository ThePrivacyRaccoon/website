# Fake Privacy Initiatives

**Because digital privacy is a social cause**, it suffers from the same issue that almost every social cause has: **people want
to make profit from it**. That leads to the creation of fake privacy initiatives, because it's almost impossible to make
profit and protect people's privacy at the same time: Have you heard the 19th century [Gold Rush](https://en.wikipedia.org/wiki/Gold_rush)?
**In the 21st century we're going through the Data Rush**. Even if a service doesn't sell your data, a government might
request it. If some organization gives privacy advise, they might be bribed by a so-called privacy defending company
and choose making money over their readers' privacy. 

Of course there are a few exceptions, but there are tons of services/organizations that claim to protect your privacy, when
reality is quite different.

## Privacy related websites

We've written brief criticism of well-known privacy sites. Our disagreement with the majority of the
"famous" privacy sites was one of the reasons for creating our site. 

Some of these projects might have
minor flaws like recommending a few less-private services/software, while other projects listed here
prioritize profit over their readers' privacy and/or include affiliated links to privacy violators and
proprietary software.

We'd like to clarify that we don't want to create hostility between privacy activists. The people behind
The Privacy Raccoon believe that we, as privacy activists, should high standards in order to provide quality recommendations
and guides. We make criticisms 
in the hope that they will be useful to improve criteria when making privacy recommendations and/or guides.
We all have the same goal, fighting back mass surveillance and teaching how to protect your digital privacy.

* [PrivacyToolsIO](privacytoolsio.html) & [PrivacyToolsIO dark patterns and the hidden contract with NordVPN.](/guides/privacytoolsio-nordvpn.html) 
* [PrivacyGuides](privacyguides.html)
* [Surveillance Report](surveillancereport.html)
* [The New Oil](the-new-oil.html)
* [Techlore](techlore.html)

## Software

* [Thunderbird](thunderbird.html)
