# About

## Anti-Copyright! Reprint freely, in any manner desired, even without naming the source

We claim no rights under the contents written here. Sharing is caring, copying is encouraged. Everything
written in this site is under the [CC0 1.0](/license.txt) unless otherwise noted.

We created this website in the hope that it will be helpful for activists, journalists and people that want
to liberate themselves from mass surveillance. We encourage you to redistribute these resources. 
Giving credits is not required but appreciated. **Please, share and copy; it's an act of love**.

## Who are you?

This wiki is maintained by volunteers, privacy activists who are outraged with corporations and governments
spying on the masses. We consider this an attack on freedom. We have to fight back.

## Why yet another site with privacy guides?

We're a group of privacy activists and computer wizards that were disappointed with "mainstream" privacy sites like
PrivacyGuides.org, PrivacyTools.io, Techlore, etc. These sites recommend solutions that are considered
spyware and even privacy violators. Read the problems with popular privacy sites [here](/the-problem.html).
They are also sold to sponsors, prioritizing money over their readers' privacy and freedom.

These sites have received a ton of attention recently. We believe that there is an increasing interest
in digital privacy. That's why we decided to create a privacy resource that truly respects your privacy and freedoms.

## Contact

By email to the main maintainer of this site, sir_reginald at the domain danwin1210.de.

## Can I help?

Of course, everyone is encouraged to help. You can contribute your own guides or recommendations. Read how to
contribute below. If you want to help maintaining or anything else, send an email.

## Contribute

It's a public wiki. Everyone can contribute using git and sending your commits. Please, abstain from posting
fake privacy initiatives. We prefer to stay away from Big Tech software too, if possible.
We value both privacy and security, but if we must choice between privacy and security,
we will always choose privacy because privacy == freedom.

### How to contribute

Read the [Contributing page](/README.md) to learn how to modify the current articles or even write your own ones.

## The Raccoon Image

The [Raccoon image](/images/raccoon.png) was extracted from the encyclopedia Meyers Konversations-Lexikon, 1896.
Under most copyright laws, due to it's age, this image is under public domain.
